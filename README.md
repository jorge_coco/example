# Git Installation

## ubuntu-18.04.4-desktop-amd64


First we update the drives and package of our ubunto system for this we go to the terminal entering root mode we use the following commands

       
       $ sudo apt-get update
       $ sudo apt-get upgrade

Then we proceed to install git with the following command

       $ sudo apt install git

To verify that the installation is correct we use the following coamndo

       $ git --version

# Git Configuration

Once git is installed we proceed to configure our environment to be able to use git by entering our username and email address with which we will connect

        $ git config --global user.name "Name"
        $ git config --global user.email "youremail@gmail.com"